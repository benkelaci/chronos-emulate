# Chronos #
## README ##

### What is this repository for? ###

Chronos is a boiling/cooling water system working on Raspberry Pi. Chronos has a web interface to control the system and tracking for the state.

![Alt text](http://i.imgur.com/8II1ydG.png "A screenshot of the Chronos web interface")
### Summary of set up ###
#### Installation ####
To install the latest version Chronos from Bitbucket repo enter the following command:
`# pip install git+https://bitbucket.org/benkelaci/chronos-emulate.git`

To install a certain version from a tag, commit or branch enter this:
`pip install git+https://bitbucket.org/benkelaci/chronos-emulate@commit|tag|branch`
#### Python packages dependencies ####

* Flask
* pyserial
* apscheduler
* pymodbus
* sqlalchemy
* python-socketio
* socketIO_client
* uwsgi
* ... (and many more)

Please install the requirements.txt (in the repo) with pip
`pip install -r requirements.txt`

#### System dependencies ####

* nginx:
`sudo apt-get install nginx`

#### Database configuration ####

the easiest way is using an existing .sql file (in the repo)

#### Deployment instructions ####

Start with pure update:
`sudo apt update && sudo apt -y dist-upgrade`


#### Files locations ####

chronos log file: `/var/log/chronos/chronos.log` (I recommend to make a symlink to a pendrive, instead of writing micro SD card)

chronos database directory: `/home/pi/chronos_db` (I recommend to create a directory symlink to a pendrive, instead of writing micro SD card)

chronos config path: `/etc/chronos_config.json`

chronos emulate files (compulsory): 
* `/home/pi/chronos_db/emulate_modbus.csv`, 
* `/home/pi/chronos_db/emulate_sensors_in.csv`, 
* `/home/pi/chronos_db/emulate_sensors_out.csv`

uwsgi files:
* `/etc/uwsgi/apps-enabled/socketio_server.ini`
* `/etc/uwsgi/apps-enabled/chronos.ini`

the chronos installed directory: `/usr/local/lib/python2.7/dist-packages/chronos`

example fstab file in the repo, required a existing `/mnt/usb` directory


#### Managing ####
Chronos had a daemon which controlled by service commands. In this version it uses `/etc/rc.local` autostart.
Service issue under investigating...

Restarting the Raspberry Pi with the `sudo ./safe_restart.sh` (if you encounter permission error, then once `sudo chmod +x safe_restart.sh`)

